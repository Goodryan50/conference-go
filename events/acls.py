import requests
import json

from .keys import PEXELS_API_KEY

def get_photo(city,state):
    print("PEXEL KEY: ", PEXELS_API_KEY)
    url="https://api.pexels.com/v1/search"
    headers= {"Authorization":PEXELS_API_KEY}
    params ={
        "query":"map of" + city +" "+ state,
        "per_page": 1
    }
    response = requests.get(url, params, headers)
    content = json.loads(response.content)
    print("***************", content)

    try:
        return {"picture_url" : content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
